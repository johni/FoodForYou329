//
//  ItemTableViewCell.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/8/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var FoodGroupLabel: UILabel!
    @IBOutlet weak var FoodNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
