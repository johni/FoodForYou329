//
//  DietSelectionTableViewCell.swift
//  FoodForYou
//
//  Created by John Ingwersen on 9/26/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit

class DietSelectionTableViewCell: UITableViewCell {
    
    static let TypesOfDiets = ["Standard", "High Protein", "Low Carb", "High Carb", "Mediterranean", "Vegetarian", "Vegan", "Create Your Own!"]

    @IBOutlet weak var TypeOfDiet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
