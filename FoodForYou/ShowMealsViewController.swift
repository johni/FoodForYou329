//
//  ShowMealsViewController.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/10/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit
import GRDB

class ShowMealsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var mealCount: Int?
    var dietName: String?
    var meals = [Int:Meal]()
    var mealArr = [Meal]()
    var grainServing = 0
    var vegServing = 0
    var fruitServing = 0
    var proteinServing = 0
    var dairyServing = 0
    var fatServing = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var DietNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.view.backgroundColor = UIColor.red
        self.DietNameLabel.text = self.dietName
        self.tableView.dataSource = self
        self.tableView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        calcServings()
        generateMeals()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func calcServings(){
        let diet = Diet.dietDictionary[self.dietName!]
        self.grainServing = (diet?.grainServing)!
        self.vegServing = (diet?.vegServing)!
        self.fruitServing = (diet?.fruitServing)!
        self.proteinServing = (diet?.proteinServing)!
        self.dairyServing = (diet?.dairyServing)!
        self.fatServing = (diet?.fatServing)!
    }
    
    func generateMeals(){
        let groupDictionary = [1:"Grains", 2:"Vegetable", 3:"Fruit", 4:"Protein", 5:"Dairy", 6:"Fats"]
        let servingDictionary = [1:self.grainServing, 2:self.vegServing, 3:self.fruitServing, 4:self.proteinServing, 5:self.dairyServing, 6:self.fatServing]
        var mealCounter:Int = 0
        while (mealCounter < self.mealCount!){
            var idServeDict = [Int:IdServing]()
            var groupCounter:Int = 1
            while (groupCounter < 7){
                
                dbQueue.inDatabase {db in
                    //create array holding all values of that group
                    let statement = "SELECT * FROM items WHERE foodGroup LIKE " + "\"%" + groupDictionary[Int(groupCounter)]! + "%\""
                    let rows = Row.fetchAll(db, statement)
                    let randomNum = Int(arc4random_uniform(UInt32(rows.count)))
                    let idVal:Int = rows[randomNum].value(named: "id")
                    let servingVal = servingDictionary[Int(groupCounter)]!
                    let value = IdServing(id: idVal, serving: servingVal)
                    idServeDict[Int(groupCounter)] = value
                   
                }
                groupCounter += 1
                
            }
            self.meals[Int(mealCounter)] = (Meal(grainID: (idServeDict[1]?.id)!, grainServing: (idServeDict[1]?.serving)!, vegID: (idServeDict[2]?.id)!, vegServing: (idServeDict[2]?.serving)!, fruitID: (idServeDict[3]?.id)!, fruitServing: (idServeDict[3]?.serving)!, proteinID: (idServeDict[4]?.id)!, proteinServing: (idServeDict[4]?.serving)!, dairyID: (idServeDict[5]?.id)!, dairyServing: (idServeDict[5]?.serving)!, fatID: (idServeDict[6]?.id)!, fatServing: (idServeDict[6]?.serving)!))
            mealCounter += 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mealCount!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MealCell", for: indexPath) as! MealCreationTableViewCell
        cell.MealNumberLabel.text = "Meal " + "\(indexPath.row+1)"
        cell.GrainServingLabel.text = "\((meals[indexPath.row]?.grainServingAmount)!)"
        cell.VegServingLabel.text = "\((meals[indexPath.row]?.vegServingAmount)!)"
        cell.FruitServingLabel.text = "\((meals[indexPath.row]?.fruitServingAmount)!)"
        cell.ProtienServingLabel.text = "\((meals[indexPath.row]?.proteinServingAmount)!)"
        cell.DairyServingLabel.text = "\((meals[indexPath.row]?.dairyServingAmount)!)"
        cell.FatServingLabel.text = "\((meals[indexPath.row]?.fatServingAmount)!)"
        //db calls to get values
        dbQueue.inDatabase{ db in
            let statement1 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row]?.grainID)!)"
            let row1 = Row.fetchOne(db, statement1)
            cell.GrainLabel.text = row1?.value(named: "foodName")
            let statement2 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row]?.vegID)!)"
            let row2 = Row.fetchOne(db, statement2)
            cell.VegLabel.text = row2?.value(named: "foodName")
            let statement3 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row]?.fruitID)!)"
            let row3 = Row.fetchOne(db, statement3)
            cell.FruitLabel.text = row3?.value(named: "foodName")
            let statement4 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row]?.proteinID)!)"
            let row4 = Row.fetchOne(db, statement4)
            cell.ProteinLabel.text = row4?.value(named: "foodName")
            let statement5 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row]?.dairyID)!)"
            let row5 = Row.fetchOne(db, statement5)
            cell.DairyLabel.text = row5?.value(named: "foodName")
            if (meals[indexPath.row]?.fatServingAmount)! == 0 {
                cell.FatLabel.text = "None"
            } else {
                let statement6 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row]?.fatID)!)"
                let row6 = Row.fetchOne(db, statement6)
                cell.FatLabel.text = row6?.value(named: "foodName")
            }
            




        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertController = UIAlertController(title: "Would you like to save this meal?", message: "You can save this meal for future use.", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.saveMealToMemory(index: indexPath.row)
            }
        )
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func saveMealToMemory(index: Int){
        var i = 0
        while i < self.meals.count {
            self.mealArr.append(self.meals[i]!)
            i += 1
        }
        if let savedMeals = loadMeals() {
            self.mealArr += savedMeals
        }
        let isSuccessful = NSKeyedArchiver.archiveRootObject(self.mealArr, toFile: Meal.archiveURL)
        if  !isSuccessful {
            print("Failed to save meals.")
        }
    }
    
    func loadMeals() -> [Meal]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Meal.archiveURL) as! [Meal]?
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
