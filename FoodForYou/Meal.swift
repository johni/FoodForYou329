//
//  Meal.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/11/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import Foundation

class Meal: NSObject, NSCoding {
    
    static let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
    static let archiveURL = documentsPath.appendingPathComponent("meals")
    
    let grainID: Int?
    let grainServingAmount: Int?
    let vegID: Int?
    let vegServingAmount: Int?
    let fruitID: Int?
    let fruitServingAmount: Int?
    let proteinID: Int?
    let proteinServingAmount: Int?
    let dairyID: Int?
    let dairyServingAmount: Int?
    let fatID: Int?
    let fatServingAmount: Int?
    
    init(grainID: Int, grainServing: Int, vegID: Int, vegServing: Int, fruitID: Int, fruitServing: Int, proteinID: Int, proteinServing: Int, dairyID: Int, dairyServing: Int, fatID: Int, fatServing: Int){
        
        self.grainID = grainID
        self.grainServingAmount = grainServing
        self.vegID = vegID
        self.vegServingAmount = vegServing
        self.fruitID = fruitID
        self.fruitServingAmount = fruitServing
        self.proteinID = proteinID
        self.proteinServingAmount = proteinServing
        self.dairyID = dairyID
        self.dairyServingAmount = dairyServing
        self.fatID = fatID
        self.fatServingAmount = fatServing
        super.init()
        
    }
    
    struct PropertyKey {
        static let grainIdKey = "grainId"
        static let grainServingKey = "grainServing"
        static let vegIdKey = "vegId"
        static let vegServingKey = "vegServing"
        static let fruitIdKey = "fruitId"
        static let fruitServingKey = "fruitServing"
        static let proteinIdKey = "proteinId"
        static let proteinServingKey = "proteinServing"
        static let dairyIdKey = "dairyId"
        static let dairyServingKey = "dairyServing"
        static let fatIDKey = "fatId"
        static let fatServingKey = "fatServing"
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(grainID, forKey: PropertyKey.grainIdKey)
        aCoder.encode(grainServingAmount, forKey: PropertyKey.grainServingKey)
        aCoder.encode(vegID, forKey: PropertyKey.vegIdKey)
        aCoder.encode(vegServingAmount, forKey: PropertyKey.vegServingKey)
        aCoder.encode(fruitID, forKey: PropertyKey.fruitIdKey)
        aCoder.encode(fruitServingAmount, forKey: PropertyKey.fruitServingKey)
        aCoder.encode(proteinID, forKey: PropertyKey.proteinIdKey)
        aCoder.encode(proteinServingAmount, forKey: PropertyKey.proteinServingKey)
        aCoder.encode(dairyID, forKey: PropertyKey.dairyIdKey)
        aCoder.encode(dairyServingAmount, forKey: PropertyKey.dairyServingKey)
        aCoder.encode(fatID, forKey: PropertyKey.fatIDKey)
        aCoder.encode(fatServingAmount, forKey: PropertyKey.fatServingKey)
        
    }
    
    required convenience init(coder aDecoder: NSCoder){
        let grainID = (aDecoder.decodeObject(forKey: PropertyKey.grainIdKey))
        let grainServing = aDecoder.decodeObject(forKey: PropertyKey.grainServingKey)
        let vegID = aDecoder.decodeObject(forKey: PropertyKey.vegIdKey)
        let vegServing = aDecoder.decodeObject(forKey: PropertyKey.vegServingKey)
        let fruitID = aDecoder.decodeObject(forKey: PropertyKey.fruitIdKey)
        let fruitServing = aDecoder.decodeObject(forKey: PropertyKey.fruitServingKey)
        let proteinID = aDecoder.decodeObject(forKey: PropertyKey.proteinIdKey)
        let proteinServing = aDecoder.decodeObject(forKey: PropertyKey.proteinServingKey)
        let dairyID = aDecoder.decodeObject(forKey: PropertyKey.dairyIdKey)
        let dairyServing = aDecoder.decodeObject(forKey: PropertyKey.dairyServingKey)
        let fatID = aDecoder.decodeObject(forKey: PropertyKey.fatIDKey)
        let fatServing = aDecoder.decodeObject(forKey: PropertyKey.fatServingKey)
        self.init(grainID: grainID as! Int, grainServing: grainServing as! Int, vegID: vegID as! Int, vegServing: vegServing as! Int, fruitID: fruitID as! Int, fruitServing: fruitServing as! Int, proteinID: proteinID as! Int, proteinServing: proteinServing as! Int, dairyID: dairyID as! Int, dairyServing: dairyServing as! Int, fatID: fatID as! Int, fatServing: fatServing as! Int)
    }
    
}
