//
//  YourCreationViewController.swift
//  FoodForYou
//
//  Created by John Ingwersen on 9/26/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit

class YourCreationViewController: UIViewController {

    var grainServing = 0
    var vegServing = 0
    var fruitServing = 0
    var proteinServing = 0
    var dairyServing = 0
    var fatServing = 0
    var totalServings = 0
    
    
    @IBAction func createButton(_ sender: UIButton) {
        let customDiet = Diet(gPerc: self.grainServing, fruPerc: self.fruitServing, vPerc: self.vegServing, pPerc: self.proteinServing, dPerc: self.dairyServing, fatPerc: self.fatServing)
        Diet.dietDictionary["custom"] = customDiet
        performSegue(withIdentifier: "createCustom", sender: self)
    }
    @IBOutlet weak var totalServingsLabel: UILabel!
    
    @IBAction func proteinHandler(_ sender: UITextField) {
        if let value = proteinServingField.text{
            if (value != ""){
                self.proteinServing = Int(value)!
                calcTotalServings()
            } else {
                self.proteinServing = 0
                calcTotalServings()
            }
        }
    }
    @IBAction func fruitHandler(_ sender: UITextField) {
        if let value = fruitServingField.text{
            if (value != ""){
                self.fruitServing = Int(value)!
                calcTotalServings()
            } else {
                self.fruitServing = 0
                calcTotalServings()
            }
        }
    }
    @IBAction func vegHandler(_ sender: UITextField) {
        if let value = vegServingField.text{
            if (value != ""){
                self.vegServing = Int(value)!
                calcTotalServings()
            } else {
                self.vegServing = 0
                calcTotalServings()
            }
        }
    }
    @IBAction func grainHandler(_ sender: UITextField) {
        if let value = grainServingField.text{
            if (value != ""){
                self.grainServing = Int(value)!
                calcTotalServings()
            } else {
                self.grainServing = 0
                calcTotalServings()
            }
        }
    }
    @IBAction func dairyHandler(_ sender: UITextField) {
        if let value = dairyServingField.text{
            if (value != ""){
                self.dairyServing = Int(value)!
                calcTotalServings()
            } else {
                self.dairyServing = 0
                calcTotalServings()
            }
        }
    }
    @IBAction func fatsHandler(_ sender: UITextField) {
        if let value = fatsServingField.text{
            if (value != ""){
                self.fatServing = Int(value)!
                calcTotalServings()
            } else {
                self.fatServing = 0
                calcTotalServings()
            }
        }
    }
    
    
    @IBOutlet weak var fatsServingField: UITextField!
    @IBOutlet weak var dairyServingField: UITextField!
    @IBOutlet weak var proteinServingField: UITextField!
    @IBOutlet weak var fruitServingField: UITextField!
    @IBOutlet weak var vegServingField: UITextField!
    @IBOutlet weak var grainServingField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blue
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func calcTotalServings(){
        self.totalServings = self.grainServing + self.vegServing + self.fruitServing + self.proteinServing + self.dairyServing + self.fatServing
        self.totalServingsLabel.text = String(self.totalServings)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "createCustom" {
            if let destinationVC = segue.destination as? ShowMealsViewController {
                destinationVC.dietName = "custom"
                destinationVC.mealCount = 1
            }
        }

    }
    

}
