//
//  OurCreationViewController.swift
//  FoodForYou
//
//  Created by John Ingwersen on 9/26/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit

class OurCreationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private var NameToPass = ""
    private var dietToPass = ""
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.red
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.textView.allowsEditingTextAttributes = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var tableView: UITableView!

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DietSelection", for: indexPath) as! DietSelectionTableViewCell
        cell.TypeOfDiet.text = DietSelectionTableViewCell.TypesOfDiets[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DietSelectionTableViewCell.TypesOfDiets.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.NameToPass = DietSelectionTableViewCell.TypesOfDiets[indexPath.row]
        if (self.NameToPass != "Create Your Own!"){
            self.performSegue(withIdentifier: "DietInformation", sender: self)
        } else {
            self.performSegue(withIdentifier: "createDiet", sender: self)
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DietInformation" {
            if let destinationVC = segue.destination as? DietInformationViewController {
                destinationVC.DietTypeLabelText = self.NameToPass
            }
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
