//
//  IdServing.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/11/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import Foundation

class IdServing{
    let id: Int?
    let serving: Int?
    
    init(id: Int, serving: Int){
        self.id = id
        self.serving = serving
    }
}
