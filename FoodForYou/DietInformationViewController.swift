//
//  DietInformationViewController.swift
//  FoodForYou
//
//  Created by John Ingwersen on 9/27/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit

class DietInformationViewController: UIViewController {

    @IBOutlet weak var DietTypeLabel: UILabel!
    var DietTypeLabelText: String?
    var currentDiet : Diet?
    var mealCount = 0;
    
    @IBOutlet weak var TotalServingsLabel: UILabel!
    @IBOutlet weak var FatsNumberLabel: UILabel!
    @IBOutlet weak var ProteinNumberLabel: UILabel!
    @IBOutlet weak var VeggiesNumberLabel: UILabel!
    @IBOutlet weak var DairyNumberLabel: UILabel!
    @IBOutlet weak var FruitsNumberLabel: UILabel!
    @IBOutlet weak var GrainsNumberLabel: UILabel!
    
    
    @IBAction func ThreeMealsButton(_ sender: UIButton) {
        self.mealCount = 3
        performSegue(withIdentifier: "MealCreation", sender: self)
    }
    @IBAction func OneMealButton(_ sender: UIButton) {
        self.mealCount = 1
        performSegue(withIdentifier: "MealCreation", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.red
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.DietTypeLabel.text = DietTypeLabelText
        self.currentDiet = Diet.dietDictionary[DietTypeLabelText!]
        setupDietLabels()
        changeTotalServings()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDietLabels(){
        self.GrainsNumberLabel.text = "\((self.currentDiet?.grainServing)!)"
        self.VeggiesNumberLabel.text = "\((self.currentDiet?.vegServing)!)"
        self.FruitsNumberLabel.text = "\((self.currentDiet?.fruitServing)!)"
        self.ProteinNumberLabel.text = "\((self.currentDiet?.proteinServing)!)"
        self.DairyNumberLabel.text = "\((self.currentDiet?.dairyServing)!)"
        self.FatsNumberLabel.text = "\((self.currentDiet?.fatServing)!)"
    }
    
    func changeTotalServings(){
        let amount = Int(self.GrainsNumberLabel.text!)!+Int(self.VeggiesNumberLabel.text!)!+Int(self.FruitsNumberLabel.text!)!+Int(self.ProteinNumberLabel.text!)!+Int(self.DairyNumberLabel.text!)!+Int(self.FatsNumberLabel.text!)!
        self.TotalServingsLabel.text = "\(amount)"
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MealCreation" {
            if let destinationVC = segue.destination as? ShowMealsViewController {
                destinationVC.dietName = self.DietTypeLabelText
                destinationVC.mealCount = self.mealCount
            }
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
