//
//  FoodItem.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/10/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import Foundation

class FoodItem {
    let itemName:String
    let servingSize:String
    let cholesterol:Int
    let calories:Int
    let saturatedFat:Int
    let fiber:Int
    let transFat:Int
    let sugar:Int
    let protein:Int
    let carbs:Int
    let foodGroup:String
    
    init(name: String, servingSize: String, cholest: Int, calories:Int, satFat:Int, fiber:Int,transFat:Int, sugar:Int, protein:Int, carbs:Int, group:String){
        self.itemName = name
        self.servingSize = servingSize
        self.cholesterol = cholest
        self.calories = calories
        self.saturatedFat = satFat
        self.fiber = fiber
        self.transFat = transFat
        self.sugar = sugar
        self.protein = protein
        self.carbs = carbs
        self.foodGroup = group
    }
    init(){
        self.itemName = "Name"
        self.servingSize = "Serving"
        self.cholesterol = 0
        self.calories = 0
        self.saturatedFat = 0
        self.fiber = 0
        self.transFat = 0
        self.sugar = 0
        self.protein = 0
        self.carbs = 0
        self.foodGroup = "Group"
    }
}
