//
//  Diet.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/4/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import Foundation

class Diet {
    
    static let dailyServings = 21
    let grainServing: Int
    let fruitServing: Int
    let vegServing: Int
    let proteinServing: Int
    let dairyServing: Int
    let fatServing: Int
    
    
    init(gPerc: Int, fruPerc: Int, vPerc: Int, pPerc: Int, dPerc: Int, fatPerc: Int){
        self.grainServing = gPerc
        self.fruitServing = fruPerc
        self.vegServing = vPerc
        self.proteinServing = pPerc
        self.dairyServing = dPerc
        self.fatServing = fatPerc
    }
    
    
    //indicates serving size per meal
    static let standard = Diet(gPerc: 3, fruPerc: 1, vPerc: 2, pPerc: 1, dPerc: 1, fatPerc: 1)
    static let highProtein = Diet(gPerc: 2, fruPerc: 1, vPerc: 2, pPerc: 2, dPerc: 2, fatPerc: 0)
    static let highCarb = Diet(gPerc:4, fruPerc: 1, vPerc: 1, pPerc: 1, dPerc: 1, fatPerc: 0)
    static let lowCarb = Diet(gPerc: 2, fruPerc: 2, vPerc: 2, pPerc: 2, dPerc: 1, fatPerc: 1)
    static let vegetarian = Diet(gPerc: 3, fruPerc: 2, vPerc: 3, pPerc: 1, dPerc: 1, fatPerc: 0)
    static let vegan = Diet(gPerc: 2, fruPerc: 2, vPerc: 3, pPerc: 1, dPerc: 0, fatPerc: 0)
    static let mediterranean = Diet(gPerc: 0, fruPerc: 0, vPerc: 0, pPerc: 0, dPerc: 0, fatPerc: 0)
    static var dietDictionary: [String:Diet] = [
        "Standard":Diet.standard,
        "High Protein":Diet.highProtein,
        "High Carb":Diet.highCarb,
        "Low Carb":Diet.lowCarb,
        "Vegetarian":Diet.vegetarian,
        "Vegan":Diet.vegan,
        "Mediterranean":Diet.mediterranean
    ]
    
}
