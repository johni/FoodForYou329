//
//  Database.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/6/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import Foundation
import GRDB

var dbQueue: DatabaseQueue!

func SetupDB() throws {
    
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
    let databasePath = documentsPath.appendingPathComponent("ffy.sqlite")
    dbQueue = try DatabaseQueue(path: databasePath)
}

func getAllItems() {
    dbQueue.inDatabase{ db in
        let rows = Row.fetchAll(db, "SELECT * FROM items")
        print(rows.count)
        print(rows.count)
    }
    
}

    

