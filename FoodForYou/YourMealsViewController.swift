//
//  YourMealsViewController.swift
//  FoodForYou
//
//  Created by John Ingwersen on 9/26/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit
import GRDB

class YourMealsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var meals = [Meal]()
    var mealCount = 0
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.green
        self.tableView.dataSource = self
        self.tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let savedMeals = loadMeals(){
            self.meals += savedMeals
            self.mealCount = self.meals.count
        } else {
            print("loaded no meals")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mealCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "YourMealCell", for: indexPath) as! MealCreationTableViewCell
        cell.MealNumberLabel.text = "Meal " + "\(indexPath.row+1)"
        cell.GrainServingLabel.text = "\((meals[indexPath.row].grainServingAmount)!)"
        cell.VegServingLabel.text = "\((meals[indexPath.row].vegServingAmount)!)"
        cell.FruitServingLabel.text = "\((meals[indexPath.row].fruitServingAmount)!)"
        cell.ProtienServingLabel.text = "\((meals[indexPath.row].proteinServingAmount)!)"
        cell.DairyServingLabel.text = "\((meals[indexPath.row].dairyServingAmount)!)"
        cell.FatServingLabel.text = "\((meals[indexPath.row].fatServingAmount)!)"
        //db calls to get values
        dbQueue.inDatabase{ db in
            let statement1 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row].grainID)!)"
            let row1 = Row.fetchOne(db, statement1)
            cell.GrainLabel.text = row1?.value(named: "foodName")
            let statement2 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row].vegID)!)"
            let row2 = Row.fetchOne(db, statement2)
            cell.VegLabel.text = row2?.value(named: "foodName")
            let statement3 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row].fruitID)!)"
            let row3 = Row.fetchOne(db, statement3)
            cell.FruitLabel.text = row3?.value(named: "foodName")
            let statement4 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row].proteinID)!)"
            let row4 = Row.fetchOne(db, statement4)
            cell.ProteinLabel.text = row4?.value(named: "foodName")
            let statement5 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row].dairyID)!)"
            let row5 = Row.fetchOne(db, statement5)
            cell.DairyLabel.text = row5?.value(named: "foodName")
            if (meals[indexPath.row].fatServingAmount)! == 0 {
                cell.FatLabel.text = "None"
            } else {
                let statement6 = "Select * FROM items WHERE id = " + "\((self.meals[indexPath.row].fatID)!)"
                let row6 = Row.fetchOne(db, statement6)
                cell.FatLabel.text = row6?.value(named: "foodName")
            }
            
        }
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertController = UIAlertController(title: "Would you like to delete this meal?", message: "You will not be able to recover the meal.", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.meals.remove(at: indexPath.row)
            self.mealCount -= 1
            self.saveMeals()
            self.tableView.reloadData()
            }
        )
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func loadMeals() -> [Meal]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Meal.archiveURL) as! [Meal]?
    }
    
    func saveMeals(){
        let isSuccessful = NSKeyedArchiver.archiveRootObject(self.meals, toFile: Meal.archiveURL)
        if !isSuccessful {
            print("Unable to save.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
