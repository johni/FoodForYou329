//
//  MealCreationTableViewCell.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/10/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit

class MealCreationTableViewCell: UITableViewCell {

    
    @IBOutlet weak var FatServingLabel: UILabel!
    @IBOutlet weak var DairyServingLabel: UILabel!
    @IBOutlet weak var ProtienServingLabel: UILabel!
    @IBOutlet weak var FruitServingLabel: UILabel!
    @IBOutlet weak var VegServingLabel: UILabel!
    @IBOutlet weak var GrainServingLabel: UILabel!
    @IBOutlet weak var FatLabel: UILabel!
    @IBOutlet weak var DairyLabel: UILabel!
    @IBOutlet weak var ProteinLabel: UILabel!
    @IBOutlet weak var FruitLabel: UILabel!
    @IBOutlet weak var VegLabel: UILabel!
    @IBOutlet weak var GrainLabel: UILabel!
    @IBOutlet weak var MealNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
