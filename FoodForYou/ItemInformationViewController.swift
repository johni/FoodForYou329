//
//  ItemInformationViewController.swift
//  FoodForYou
//
//  Created by John Ingwersen on 10/10/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit

class ItemInformationViewController: UIViewController {

    @IBOutlet weak var FoodGroupLabel: UILabel!
    @IBOutlet weak var CarbValueLabel: UILabel!
    @IBOutlet weak var ProteinValueLabel: UILabel!
    @IBOutlet weak var SugarValueLabel: UILabel!
    @IBOutlet weak var FiberValueLabel: UILabel!
    @IBOutlet weak var TransFatValueLabel: UILabel!
    @IBOutlet weak var SatFatValueLabel: UILabel!
    @IBOutlet weak var CaloriesValueLabel: UILabel!
    @IBOutlet weak var CholesterolValueLabel: UILabel!
    @IBOutlet weak var ServingSizeLabel: UILabel!
    @IBOutlet weak var ItemNameLabel: UILabel!
    
    var item: FoodItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.yellow
        setupLabels()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLabels(){
        //"\((self.currentDiet?.grainServing)!)"
        self.FoodGroupLabel.text = item?.foodGroup
        self.CarbValueLabel.text = "\((item?.carbs)!)"
        self.ProteinValueLabel.text = "\((item?.protein)!)"
        self.SugarValueLabel.text = "\((item?.sugar)!)"
        self.FiberValueLabel.text = "\((item?.fiber)!)"
        self.TransFatValueLabel.text = "\((item?.transFat)!)"
        self.SatFatValueLabel.text = "\((item?.saturatedFat)!)"
        self.CaloriesValueLabel.text = "\((item?.calories)!)"
        self.CholesterolValueLabel.text = "\((item?.cholesterol)!)"
        self.ServingSizeLabel.text = "\((item?.servingSize)!)"
        self.ItemNameLabel.text = item?.itemName
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
