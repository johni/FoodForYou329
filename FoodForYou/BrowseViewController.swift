//
//  BrowseViewController.swift
//  FoodForYou
//
//  Created by John Ingwersen on 9/26/16.
//  Copyright © 2016 SE329. All rights reserved.
//

import UIKit
import GRDB

class BrowseViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    
    @IBAction func FoodGroupSortButton(_ sender: UIButton) {
        if currentSortSelection{
            if currentSortDirection {
                self.currentSortDirection = false
                setupBrowse(column: "FoodGroup", order: "DESC")
            } else {
                self.currentSortDirection = true
                setupBrowse(column: "FoodGroup", order: "ASC")
                
            }
        } else {
            self.currentSortDirection = true
            self.currentSortSelection = false
            setupBrowse(column: "FoodGroup", order: "ASC")
        }
    }
    
    @IBAction func FoodNameSortButton(_ sender: UIButton) {
        if currentSortSelection{
            if currentSortDirection {
                self.currentSortDirection = false
                setupBrowse(column: "FoodName", order: "DESC")
            } else {
                self.currentSortDirection = true
                setupBrowse(column: "FoodName", order: "ASC")

            }
        } else {
            self.currentSortDirection = true
            self.currentSortSelection = true
            setupBrowse(column: "FoodName", order: "ASC")
        }
    }
    
    @IBOutlet weak var textView: UITextView!
    var currentSortSelection = true //true is FoodName, false is FoodGroup
    var currentSortDirection = true //true is ASC, false is DESC
    
    var itemToPass:FoodItem = FoodItem.init()
    var rowCount = 10
    var rows: [GRDB.Row]?
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.yellow
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.textView.allowsEditingTextAttributes = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBrowse(column: "FoodName", order: "ASC")
        
    }

    func setupBrowse(column: String, order: String){
        dbQueue.inDatabase { db in
            self.rows = Row.fetchAll(db, "SELECT * FROM items ORDER BY " + column + " " + order)
            self.rowCount = (rows?.count)!
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemTableViewCell
        cell.FoodNameLabel.text = self.rows?[indexPath.row].value(named: "FoodName")
        cell.FoodGroupLabel.text = self.rows?[indexPath.row].value(named: "FoodGroup")
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        constructItemToPass(index: indexPath.row)
        performSegue(withIdentifier: "MoveToItemInfo", sender: self)
    }

    func constructItemToPass(index: Int){
        self.itemToPass = FoodItem.init(name: (self.rows?[index].value(named: "foodName"))!, servingSize: (self.rows?[index].value(named: "servingSize"))!, cholest: (self.rows?[index].value(named: "cholesterol"))!, calories: (self.rows?[index].value(named: "calories"))!, satFat: (self.rows?[index].value(named: "saturatedFat"))!, fiber: (self.rows?[index].value(named: "fiber"))!, transFat: (self.rows?[index].value(named: "transFat"))!, sugar: (self.rows?[index].value(named: "sugar"))!, protein: (self.rows?[index].value(named: "protein"))!, carbs: (self.rows?[index].value(named: "carbs"))!, group: (self.rows?[index].value(named: "foodGroup"))!)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MoveToItemInfo" {
            if let destinationVC = segue.destination as? ItemInformationViewController {
                destinationVC.item = self.itemToPass
            }
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
