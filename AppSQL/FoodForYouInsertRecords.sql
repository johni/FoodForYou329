insert into items(id, foodName, foodGroup, calories, protein, sugar, saturatedFat, transFat, carbs, fiber, cholesterol)
values 
		(1,"Multigrain Oatmeal", "Grains", 133, 4.52, 0.24, 0.21, 0, 29.38, 4.8, 0),
		(2,"Brown Rice", "Grains", 362, 8, 0, 1, 0, 76, 3, 0),
        (3,"Wheat Bread", "Grains", 76, 4.07, 1.44, 0.23, 0, 12.79, 2.3, 0),
        (4,"Quinoa", "Grains", 222, 8, 0, 0, 0, 39, 5, 0),
        (5,"Wild Rice", "Grains", 160, 5, 1, 0, 0, 33, 2, 0),
        (6,"Whole Grain Penne","Grains", 210, 9, 3.57, 0, 0, 40, 6, 0),
        (7,"Whole Grain Spaghetti", "Grains", 210, 7, 2, 0, 0, 42, 5, 0),
        (8,"Whole Wheat Pancakes", "Grains", 120, 4, 4, 0, 0, 26, 3, 0),
        (9,"Whole Wheat English Muffin", "Grains", 58, 2.49, 2.29, 0.1, 0, 11.45, 1.9, 0),
        (10,"Blueberry Muffin", "Grains", 250, 2, 21, 2, 0, 40, 2, 25),
        (11,"Whole Wheat Buttermilk Biscuit", "Grains", 170, 4, 2, 2, 3.5, 23, 1, 0);
     
-- Select * from items;
-- 
-- ALTER TABLE items
-- Modify COLUMN foodName varchar(40);