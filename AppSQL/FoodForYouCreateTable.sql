use dbu329jh01;

CREATE TABLE items(
	id int,
	foodName VARCHAR(40),
    foodGroup VARCHAR(20),
    calories int,
    protein int,
    sugar int,
    saturatedFat int,
    transFat int,
    carbs int,
    fiber int,
    cholesterol int,
    servingSize int,
    PRIMARY KEY (id)
);